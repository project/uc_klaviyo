<?php

/**
 * @file
 * Contains uc_klaviyo.module.
 */

/**
 * Implements hook_menu().
 */
function uc_klaviyo_menu() {
  $items['admin/klaviyo/sync/order/%'] = array(
    'title' => 'Klaviyo - Sync order',
    'page callback' => 'uc_klaviyo_sync_order',
    'page arguments' => array(4),
    'access callback' => 'user_access',
    'access arguments' => array('manage sync klaviyo order'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function uc_klaviyo_permission() {
  return array(
    'manage sync klaviyo order' => array(
      'title' => t('Klaviyo - Sync order'),
    ),
  );
}

/**
 * Function to sync order with Klaviyo.
 */
function uc_klaviyo_sync_order($order_id) {

  if (!empty($order_id) && is_numeric($order_id)) {

    // Load Order object.
    $order = uc_order_load($order_id);

    $products = array();

    foreach ($order->products as $product) {
      $products[] = array(
        'Name' => $product->title,
        'SKU' => $product->model,
        'price' => $product->price,
        'qty' => ($product->qty) ? $product->qty : 1,
      );
    }

    $request = array('Purchased item',
      array(
        '$email' => $order->primary_email,
        '$first_name' => $order->delivery_first_name,
        '$last_name' => $order->delivery_last_name,
      ),
      array(
        'Products' => $products,
        'Order ID' => $order->order_id,
        'Payment Method' => $order->payment_method,
        'Product price total' => uc_order_get_total($order, TRUE),
        'Grand Total' => uc_order_get_total($order),
      ),
      $order->created,
    );
    // Syc with Klaviyo.
    try {
      $api_key = variable_get('klaviyo_api_key', '');
      $klaviyo = new PHPKlaviyo($api_key);
      $klaviyo->track($request[0], $request[1], $request[2], $request[3]);
    }
    catch (Exception $e) {
      // $e->getMessage();.
      watchdog('uc_klaviyo', print_r($request, TRUE));
      watchdog('uc_klaviyo', 'Exception : Unable to send to Klaviyo.');
    }

    drupal_set_message(t('Synced order with Kalviyo.'), 'status');
  }

  drupal_goto('/admin');
}

/**
 * Implements hook_uc_checkout_complete().
 *
 * Synchronise order with Klaviyo account.
 */
function uc_klaviyo_uc_checkout_complete($order, $account) {

  $products = array();

  foreach ($order->products as $product) {
    $products[] = array(
      'Name' => $product->title,
      'SKU' => $product->model,
      'price' => $product->price,
      'qty' => ($product->qty) ? $product->qty : 1,
    );
  }

  $request = array('Purchased item',
    array(
      '$email' => $order->primary_email,
      '$first_name' => $order->delivery_first_name,
      '$last_name' => $order->delivery_last_name,
    ),
    array(
      'Products' => $products,
      'Order ID' => $order->order_id,
      'Payment Method' => $order->payment_method,
      'Product price total' => uc_order_get_total($order, TRUE),
      'Grand Total' => uc_order_get_total($order),
    ),
    $order->created,
  );

  watchdog('uc_klaviyo', print_r($request, TRUE));
  // Syc with Klaviyo.
  try {
    $api_key = variable_get('klaviyo_api_key', '');
    $klaviyo = new PHPKlaviyo($api_key);
    $klaviyo->track($request[0], $request[1], $request[2], $request[3]);
  }
  catch (Exception $e) {
    // $e->getMessage();.
    watchdog('uc_klaviyo', 'Exception : Unable to send to Klaviyo.');
  }
}

/**
 * Implements hook_preprocess_html().
 * API Documentation https://help.klaviyo.com/hc/en-us/articles/115005082927-Integrate-a-Custom-Ecommerce-Cart-or-Platform
 */
function uc_klaviyo_preprocess_html(&$vars) {

  $api_key = variable_get('klaviyo_api_key', '');
  global $user;
  global $base_url;
  $script_lib = "\n<script>
  var _learnq = _learnq || [];
  _learnq.push(['account', '" . $api_key . "']);
  (function () {
    var b = document.createElement('script'); b.type = 'text/javascript'; b.async = true;
    b.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'a.klaviyo.com/media/js/analytics/analytics.js';
    var a = document.getElementsByTagName('script')[0]; a.parentNode.insertBefore(b, a);
  })();
</script>";

  $element = array(
    '#type' => 'markup',
    '#markup' => $script_lib,
    '#weight' => 1000,
  );
  drupal_add_html_head($element, 'uc_klaviyo_lib');

  // If visitors or customers can create accounts for your store, add the following snippet directly below the first snippet.
  if ($user->uid != 0) {
    // User is logged in.
    $script_user = "\n<script>
  var _learnq = _learnq || [];
  _learnq.push(['identify', {
    " . '$email' . ": '" . $user->mail . "',
  }]);
</script>";
    $element = array(
      '#type' => 'markup',
      '#markup' => $script_user,
      '#weight' => 1001,
    );
    drupal_add_html_head($element, 'uc_klaviyo_user');
  }

  // START: Track Started Checkout.
  if (arg(0) == 'cart' && arg(1) == 'checkout' && arg(2) == NULL) {
    $order = uc_order_load($_SESSION['cart_order']);
    $order_total = $order->order_total;
    $items = array();
    $item_names = array();
    foreach ($order->products as $product) {
      $item['SKU'] = $product->model;
      $item['ProductName'] = $product->title;
      $item['Quantity'] = $product->qty;
      $item['ItemPrice'] = $product->price;
      $item['RowTotal'] = $product->price * $product->qty;
      //$item['ProductURL'] = '';
      //$item['ImageURL'] = '';
      //$item['ProductCategories'] = '';
      $items[] = $item;
      $item_names[] = $product->title;
    }

    $script_started_checkout = '\n<script text="text/javascript">
    var _learnq = _learnq || [];
    _learnq.push([\'track\', \'Started Checkout\', {
      "$value" : ' . $order_total . ',
      "ItemNames" : ' . json_encode($item_names) . ',
      "CheckoutURL": "' . $base_url . '/cart/checkout",
      "Items" : ' . json_encode($items) . '
    }]);
   </script>';

    $element = array(
      '#type' => 'markup',
      '#markup' => $script_started_checkout,
      '#weight' => 1002,
    );
    drupal_add_html_head($element, 'uc_klaviyo_started_checkout');

  }
  // END: Track Started Checkout.
}

/**
 * Include hook_node_view().
 * API Documentation https://help.klaviyo.com/hc/en-us/articles/115005082927-Integrate-a-Custom-Ecommerce-Cart-or-Platform
 */
function uc_klaviyo_node_view($node, $view_mode, $langcode) {
  global $base_url;
  global $base_path;
  if ($node->type == "product" && $view_mode == "full") {
    $res = db_query("SELECT * FROM {uc_products} WHERE nid =:nid", array(':nid' => $node->nid));
    $product = $res->fetchObject();

    $script_viewed = "\n<script text='text/javascript'>
    var _learnq = _learnq || [];
    var item = {
      ProductName: '" . $node->title . "',
      ProductID: " . $node->nid . ",
      URL: '" . $base_url . $base_path . request_path() . "',
      Brand: 'Audeze',
      Price: " . $product->sell_price . ",
    };

    _learnq.push(['track', 'Viewed Product', item]);

    _learnq.push(['trackViewedItem', {
      Title: item.ProductName,
      ItemId: item.ProductID,
      Url: item.URL,
      Metadata: {
        Brand: item.Brand,
        Price: item.Price,
      }
    }]);
    </script>";

    $element = array(
      '#type' => 'markup',
      '#markup' => $script_viewed,
      '#weight' => 1002,
    );
    drupal_add_html_head($element, 'uc_klaviyo_product');

    // Add to Cart Event.
    //  @refer - https://help.klaviyo.com/hc/en-us/articles/115001396711-Create-a-Custom-Add-to-Cart-Event-for-Shopify

    drupal_add_js("
  var _learnq = _learnq || [];
      document.getElementById('buy-now-top').addEventListener('click',function (){
         _learnq.push(['track', 'Add To Cart', item]);
      });
  ", array(
    'type' => 'inline',
    'weight' => 10000, // Something higher than the weight of existing items.
    'scope' => 'footer', // Make sure the script tag is rendered in the footer of the page, not the header.
    'group' => JS_THEME, // JS_THEME has the highest group weight and will be rendered last.
  ));

  }
}
