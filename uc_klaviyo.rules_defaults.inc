<?php

/**
 * @file
 * Rules default configurations for uc_klaviyo.module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_klaviyo_default_rules_configuration() {
  $configs = array();
  $rule = rules_reaction_rule();
  $rule->label = t('Synchronise order with Klaviyo.');
  $rule->active = FALSE;
  $rule->event('uc_checkout_complete')
    ->action('uc_klaviyo_action_syc_order', array('order:select' => 'order'));
  $configs['uc_klaviyo_syc_order'] = $rule;
  
  return $configs;
}
