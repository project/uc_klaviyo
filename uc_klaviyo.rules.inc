<?php

/**
 * @file
 * Rules hooks for custom.module.
 */

/**
 * Implements hook_rules_action_info().
 */
function uc_klaviyo_rules_action_info() {
  $actions['uc_klaviyo_action_syc_order'] = array(
    'label' => t('Synchronise order with Klaviyo.'),
    'group' => t('Stock'),
    'base' => 'uc_stock_action_decrement_stock',
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
      ),
    ),
  );

  return $actions;
}

/**
 * Invoke the methoud for synchronise order with Klaviyo account.
 */
function uc_klaviyo_action_syc_order($order) {
  if (is_array($order->products)) {
    uc_klaviyo_sync_order($order);
  }
}